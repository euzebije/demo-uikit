//
//  IDViewController.h
//  DynamicsPlayground
//
//  Created by Ivan Fabijanovic on 11/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IDViewController : UIViewController <UICollisionBehaviorDelegate>

@end
