//
//  main.m
//  DynamicsPlayground
//
//  Created by Ivan Fabijanovic on 11/03/14.
//  Copyright (c) 2014 Interesting Development. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IDAppDelegate class]));
    }
}
